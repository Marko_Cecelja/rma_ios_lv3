//
//  NetworkManager.swift
//  University
//
//  Created by Student on 11.05.2021..
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NetworkManager {

    static func fetchData(success: @escaping (([University]) -> Void), failure: @escaping ((Error) -> Void)) {
        let url = "http://universities.hipolabs.com/search?country=croatia"
        AF.request(url).responseJSON { r in
            
            switch r.result {
                
            case .success(let data):
                let json = JSON(data)
                
                let univs = json.arrayValue.map { j in
                    return University(with: j)
                }
                
                success(univs)
            case .failure(let error):
                failure(error)
            }
        }
    }
}

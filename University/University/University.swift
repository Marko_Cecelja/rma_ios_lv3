//
//  University.swift
//  University
//
//  Created by Student on 11.05.2021..
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit
import SwiftyJSON

class University {
    
    var name: String
    
    var country: String
    
    var webPage: URL?
    
    init(with json: JSON) {
        name = json["name"].stringValue
        country = json["country"].stringValue
        webPage = json["web_pages"][0].url
    }

}

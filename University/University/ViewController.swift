//
//  ViewController.swift
//  University
//
//  Created by Student on 11.05.2021..
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let cellReuseID: String = "displayCell"
    
    var universities: [University] = []
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self        // Do any additional setup after loading the view.
        NetworkManager.fetchData(success: { univs in
            self.universities = univs
            print(self.universities)
            self.tableView.reloadData()
        },
          failure: { error in
            print(error.localizedDescription)
        })
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return universities.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseID, for: indexPath) as! UniversityViewCellTableViewCell
    
    let university = universities[indexPath.row]
    
    cell.config(with: university.name, country: university.country, webPage:university.webPage)
    
    return cell
}
}

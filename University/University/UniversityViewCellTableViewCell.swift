//
//  UniversityViewCellTableViewCell.swift
//  University
//
//  Created by Student on 11.05.2021..
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit

class UniversityViewCellTableViewCell: UITableViewCell {

    @IBOutlet weak var uniName: UILabel!
    @IBOutlet weak var uniCountry: UILabel!
    @IBOutlet weak var uniURL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
     func config(with name: String, country: String, webPage: URL?) {
           self.uniName.text = name
           self.uniCountry.text = country
           self.uniURL.text = webPage?.absoluteString
    }
}
